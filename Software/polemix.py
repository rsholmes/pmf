#!/usr/bin/python3

# display pole mixing Bode plots

# polemix.py arg [arg...]

# where arg is
#     '[list] label'  or   '[list]' (label defaults to 'list')
# list being coefficients of input, -lp1, lp2, -lp3, lp4...
#
# or
#
#     a[:path] or b[:path] or p[:path]
# indicates break, show the preceding plots if any in one figure and start
# collecting next plots into a new figure. If a collect only amplitude plot,
# if p collect only phase plot, if b collect both plots. Default behavior is
# equivalent to b as last arg. If path is given write plot to that path.

import matplotlib.pyplot as plt
import numpy as np
import sys
import re

def Hn(f, fc, n):
    # Low pass transfer function for n poles, input frequency f,
    # corner frequency fc
    w = 2*np.pi*f
    wc = 2*np.pi*fc
    return (-1.0 / (1.0 + 1j * w / wc)) ** n

def H1(f, fc):
    return Hn(f, fc, 1)

def H2(f,fc):
    return Hn(f, fc, 2)

def polemix (f, fc, a):
    # Pole mixing transfer function
    pm = 0
    for ia in range(len(a)):
        pm += a[ia] * Hn (f, fc, ia)
    return pm

def usage():
    print ('Usage:\n')
    print ('python polemix.py args...\n')
    print ("where each arg is")
    print ("    '[i,l1,l2,l3,l4] label'\tSpecify a mix (label defaults to '[i,l1,l2,l3,l4]')")
    print ('    -f:<freq>              \tSpecify corner frequency for following mixes, default is 1k')
    print ('    -a[:<path>]            \tPlot amplitudes for following mixes')
    print ('    -p[:<path>]            \tPlot phases for following mixes')
    print ('    -b[:<path>]            \tPlot both for following mixes (default)')
    
    print ('                           \t<path> = path for output file, no output file if blank')

    
def main():
#    plt.rcParams["figure.figsize"] = [12, 8]  # plot size in inches
    plt.rcParams["figure.figsize"] = [7.5, 4.5]  # plot size in inches
    plt.rcParams["figure.autolayout"] = True

    f = np.logspace(1,5,num=500) # frequencies from 10**1 to 10**5

    corner = 1000
    page = [[[], [], [], [], 'b', '']] # each entry is list of coefs, list of labels, list of corners, list of polemix results, plots, path
    for a in sys.argv[1:]:

        if a == '-h':
            usage()
            return

        if a[0:3] == '-f:':
            if len(a) < 4:
                usage()
                sys.exit(1)
            corner = float(a[3:])
            continue

        if a[0:2] == '-b' or a[0:2] == '-a' or a[0:2] == '-p':
            page.append([[], [], [], [], 'b', ''])
            # Start the next figure
            path = ""
            if len(a) > 2:
                if a[2] == ':':
                    path = a[3:]
                else:
                    usage()
                    sys.exit(1)
            page[-1][4] = a[1]
            page[-1][5] = path
            continue
        
        res = re.search ('^\[(.*?)\] *(\S.*)$', a)
        if res != None and len(res.groups()) == 2:
            g1 = res.group(1)
            g2 = res.group(2)
        else:
            res = re.search ('^\[(.*?)\]$', a)
            if res and len(res.groups()) == 1:
                g1 = res.group(1)
                g2 = str(g1)
            else:
                usage()
                sys.exit(1)

        page[-1][0].append([float (i) for i in re.split (", *", g1)])
        page[-1][1].append(g2)
        page[-1][2].append(corner)
        page[-1][3].append(polemix (f, corner, page[-1][0][-1]))
        
    for j in range (len(page)):
        if len(page[j][0]) == 0:
            continue

        if page[j][4] == 'b':
            plt.subplot(211)
        if page[j][4] == 'a' or page[j][4] == 'b':
            for i in range(len(page[j][3])):
                plt.plot(f, 20*np.log10(abs(page[j][3][i])), label=page[j][1][i])
            plt.xscale('log')

            plt.xlabel('frequency (Hz)')
            plt.ylabel('amplitude (dB)')
            plt.grid(True)
            plt.legend()

        if page[j][4] == 'b':
            plt.subplot(212)
        if page[j][4] == 'p' or page[j][4] == 'b':
            for i in range(len(page[j][3])):
                plt.plot(f, 180./np.pi*np.angle(page[j][3][i]), label=page[j][1][i])
            plt.xscale('log')

            plt.xlabel('frequency (Hz)')
            plt.ylabel('phase shift (deg)')
            plt.grid(True)
            if page[j][4] == 'p':
                plt.legend()

        if page[j][5] != "":
            plt.savefig(page[j][5], dpi=150)
        plt.show()
main()
